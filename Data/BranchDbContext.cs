﻿using AS2_API.Model;
using Microsoft.EntityFrameworkCore;

namespace AS2_API.Data
{
    public class BranchDbContext:DbContext
    {
        public BranchDbContext(DbContextOptions options) : base(options)
        {
        }
        public DbSet<Branch> Branches { get; set; }
    }
}
