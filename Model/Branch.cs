﻿using System.ComponentModel.DataAnnotations;

namespace AS2_API.Model
{
    public class Branch
    {
        [Key] public int BranchId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string State { get; set; }
        [Required]
        public string Zipcode { get; set; }
    }
}
